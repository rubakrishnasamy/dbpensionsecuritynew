/**
 * Created by ruba-krishnasamy on 12/11/2017.
 */

module.exports = {
    sections: {
        LoginPagePension: {selector: 'body', elements: {
                //QAI
                PensionUsername: {selector: '#usernameId'},
                PensionPassword: {selector: '#passwordId'},
                ButtonLoginPension: {locateStrategy: 'xpath',selector: "//input[@type='submit'][@value='Submit']"},
                //LoginError: {locateStrtegy: 'xpath', selector: "//*[.='The User Name and Password entered is not valid. Please try again.']"}
                //LoginError: {locateStrtegy: 'xpath', selector: "//div[@class='messagecontent']//table//tbody//tr//td[2]"}
                //LoginError: {selector:"span[title='Warning'"}
                LoginError: {locateStrategy: 'xpath',selector: "//span[@title='Warning']/../../../../td[2]/div/table/tbody/tr/td[2]"}
            }
        },

        DashboardPension: { selector: 'body', elements: {
            DashboardTile: {locateStrategy: 'xpath', selector: "//span[contains(text(),\"Dashboard\")]"},
            DBLogoutbutton: {locateStrategy: 'xpath', selector: "//a[contains(text(),'Logout')]"},
            KelloggHeritageTile: {locateStrategy: 'xpath', selector: "//div[@class='tile-title ng-scope']//h3[contains(text(),'Kellogg Heritage')]"},
            Resumebutton: {locateStrategy: 'xpath', selector: "//div[@class='status-actions action-cancel ng-scope']//span[contains(text(),'Resume')]"}
        }
        },

        Forgotpassword: {
            selector: 'body',
            elements: {
                Title: {locateStrategy: 'xpath', selector: "//h1[contains(text(),'Forgot your User Name or Password?')]"},
                Cancelbutton: {locateStrategy: 'xpath', selector: "//input[@value='Cancel'][@type='button']"}
            }
        },

        Confirmationtab: {
            selector: 'body',
            elements:{
                Requireddocumentnav: {locateStrategy: 'xpath', selector: "//ul[@class='subnav']//a[contains(text(),'Required Documents')]"},
                Spousalconsentbox: {locateStrategy:'xpath', selector: "//*[contains(text(),'Include in Selected File')]/following::span[3]"},
                Spousalcheckbox: {locateStrategy:'xpath', selector: "//i[@class='fa fa-lg fa-square-o']"},
                Requireddocuploadtextbox: {locateStrategy:'xpath', selector: "//div[@type='text'][@class='form-control ng-binding']"},
                Requiredselectfilebutton:{locateStrategy:'xpath', selector: "//a[contains(text(),'Select File')]"},
                Requireduploadbutton:{locateStrategy:'xpath', selector: "//button[@class='btn btn-primary ng-binding']"},
                Requirederrormessage:{locateStrategy:'xpath', selector: "//span[contains(text(),'Upload unsuccessful. This file type cannot be processed. Allowed file types are PNG, PDF, GIF, TIF or TIFF, BMP, JPG, or JPEG')]"}
                }
        },

        DBProfilepage:{
            selector: 'body',
            elements:{
        MenuButtonprofile: {locateStrategy: 'xpath',selector: "//span[text()='Menu']"},
        DBProfilelink: {locateStrategy: 'xpath',selector: "//a[@title='Profile']"},
        DBEditbuttoninProfilepage: {locateStrategy: 'xpath',selector: "//span[.='Edit']"},
        DBphone: {locateStrategy: 'xpath',selector: "(//input[@name='phone'])[1]"},
        DBemail: {locateStrategy: 'xpath',selector: "//input[@name='email']"}
        }
    }

    }

};