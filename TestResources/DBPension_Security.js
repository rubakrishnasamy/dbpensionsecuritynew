/**
 * Created by ruba-krishnasamy on 12/11/2017.
 */

module.exports = {

    //Specify Your Testing Environment

    //TestingEnvironment: 'QAF',
       TestingEnvironmentPension: 'QAI',
    //TestingEnvironment: 'CITEST',
    //TestingEnvironment: 'PROD',
    //TestingEnvironment: 'CSO',

    shortWait: 5000,
    longWait: 40000,
    LoginWait: 30000,
    ProfileCreationTime: 60000,

    //Environment User details

    //QAF
    //PensionURL: 'https://ben-qaf3.mercerbenefitscentral.com/dashboard?clientid=MBCKAI&employeeId=745141625',

    //QAI
    PensionURL: 'https://auth-qai.mercerbenefitscentral.com/MBCKG1/login.tpz',
    PensionURLnonsecured: 'http://auth-qai.mercerbenefitscentral.com/MBCKG1/login.tpz',
    PensionUsername: 'test2138',
    PensionPassword: 'test0001',

    //QAI
    IncorrectPensionUsername: 'test213867',
    IncorrectPensionPassword: 'test000001',

    //QAI
    DefaultPensionUsername: '745242138',
    DefaultPensionPassword: '09231956',

    //QAI
    LockoutPensionUsername: 'test2103',
    LockoutPensionPassword: 'test000001',

    //uploadfile
    uploadPensionUsername: 'test2138',
    uploadPensionPassword: 'test0001',
    SQLInjectionpassword: '1=1',
    Jaggingfile: 'C:\\Users\\Ruba-Krishnasamy\\Desktop\\Jagging.html',
    DBPhonetext: '<body onload=alert(\'test1\')>;\n'+'<b onmouseover=alert(\'Wufff!\')>click me!</b>'


};