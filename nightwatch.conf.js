var seleniumServer = require('selenium-server');
var chromedriver = require('chromedriver');
var data = require('./TestResources/GlobalTestData');
require('nightwatch-cucumber')({
    supportFiles: ['./utils/TestExecListener.js'],
    stepTimeout:300000,
    defaultTimeoutInterval:900000
});

module.exports = {
  output_folder: 'reports',
  custom_commands_path: '',
  custom_assertions_path: '',
  page_objects_path : "repository",
  live_output: false,
  disable_colors: false,
  selenium: {
    start_process: true,
    server_path: seleniumServer.path,
    host: '127.0.0.1',
      port: 5555,
    cli_args: {
        'webdriver.chrome.driver': chromedriver.path,
        'webdriver.ie.driver': 'C:\\Users\\Ruba-Krishnasamy\\Documents\\IEDriverServer.exe',
        'webdriver.firefox.profile': ''
    }
  },

  test_settings: {
    default : {
      launch_url: "http://localhost",
      page_objects_path : "repository",
      selenium_host: "127.0.0.1",
        selenium_port: 5555,
      silent : true,
      disable_colors: false,
        screenshots: {
            enabled: false,
            on_failure: true,
            on_error: true,
            path: 'screenshots'
        },

        desiredCapabilities: {
            browserName: 'chrome',   //firefox,internet explorer
        javascriptEnabled : true,
        acceptSslCerts : true
      }
    }
  }
};

