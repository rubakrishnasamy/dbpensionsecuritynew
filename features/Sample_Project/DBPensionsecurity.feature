Feature: To verify the security scenarios for DB Pension

  @DBsecurity @DBPensionfileupload
  Scenario: User upload the document in a incorrect format and should not be allowed
    Given User Opens the MBC Login screen - upload
    When  User upload the document in wrong format
    Then  verify the error message displayed in the screen

  @DBsecurity @DBPensionHTTPStrict
  Scenario: To open the MBC login page and reach the dashboard
    Given User Opens the Login screen
    When  User enter username and password and hit the login button
    Then  MBC Dashboard page should be displayed
    And User should logout of the application

  @DBsecurity @DBPensionErrorcode
  Scenario: User enters incorrect login credentials should be prompted with the correct error message
    Given User Opens the MBC Login screen
    When  User enter Incorrect username and password and hit the login button
    Then  Error message should be displayed

  @DBsecurity @DBPensioncookie
  Scenario: User click on back button after logout and it should remain in the login page.
    Given User Opens the Login screen cookie
    When  User reaches the MBC Dashboard screen.Logout from the application and click on browser back
    Then  User should remain in the MBC Login screen

  @DBsecurity @DBPensiontimeout
  Scenario: User click on back button after logout and it should remain in the login page.
    Given User Opens the Login screen timeout
    When  User reaches the MBC Dashboard screen,copies the URL.Logout from the application and click on browser back
    And   Paste the Dashboard URL
    Then  User should remain in the MBC Login timeout

  @DBsecurity @DBPensionDefaultcredentials
  Scenario: User enters default login credentials should be prompted with the correct error message
    Given User Opens the MBC Login screen Default credentials
    When  User enter default username and password and hit the login button
    Then  Error message should be displayed for Default credentials

  @DBsecurity @DBPensionclickjagging
  Scenario: ClickJagging of the application
    Given URL is identified for the test
    When  save the URL in the file and click on the file saved in the desired location
    Then  verify that the application is not opening in the frame

  @DBsecurity @DBPensionSQLInjection
  Scenario: SQL Injection of the application
    Given User Opens the MBC Login screen for SQL Injection
    When  User enter correct username and Injected password and hit the login button
    Then  Error message should be displayed to show SQL injection is not allowed

  @DBsecurity @DBPensionlockout
  Scenario: User enters URL without lockout test
    Given User Opens the MBC Login screen - lockout
    When  User enter invalid credential and the user is locked
    Then  User enter the same username and password user should not be allowed to enter the application

  @DBsecurity @DBPensionheaders
  Scenario: User enters URL to get response header
    Given User Opens the MBC Login screen - response
    When  Get response header
    Then  verify the status code of the response header

  @DBsecurity @DBPensioncrosssite
  Scenario: cross site of the application
    Given User login the MBC application for verifying the cross site
    When  User navigates to profile page enter the XSS script
    And verify the error message
    Then User should logout of the applications



