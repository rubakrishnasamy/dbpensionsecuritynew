var data = require('../../TestResources/DBPension_Security.js');
var Objects = require(__dirname + '/../../repository/DBPensionsecurity.js');
var action = require(__dirname + '/../../features/step_definitions/DBPensionsecurity.js');
var ks = require('node-key-sender');
//var path = require('../../Downloads');
var fs = require('fs');
//var email = require(__dirname + '/../../node_modules/emailjs');

var LoginPage, DashboardPagePension, Ucopy, ErrorPrint,ForgotpasswordPension, ConfirmationpagePension, ProfilepagePension;

function initializePageObjects(browser, callback) {
    var PensionPage = browser.page.DBPensionsecurity();
    LoginPage = PensionPage.section.LoginPagePension;
    DashboardPagePension = PensionPage.section.DashboardPension;
    ForgotpasswordPension = PensionPage.section.Forgotpassword;
    ConfirmationpagePension = PensionPage.section.Confirmationtab;
    ProfilepagePension = PensionPage.section.DBProfilepage;
    callback();
}
function urlcheck(browser, callback) {
    Ucopy = browser.getUrl();
    DashboardPagePension.waitForElementVisible('@Logoutbutton', data.shortWait)
        .click('@Logoutbutton');
    browser.pause(data.shortWait);
    LoginPage.waitForElementVisible('@PensionUsername', data.shortWait);
    browser.pause(4000);
    browser.url(Ucopy);
    callback();
}
module.exports = function() {

    this.Given(/^User Opens the Login screen$/, function () {
        var URL;
        browser = this;
        var execEnv = data["TestingEnvironmentPension"];
        console.log('Test Environment: ' + execEnv);
        if (execEnv.toUpperCase() === "QAI") {
            URL = data.PensionURL;
            initializePageObjects(browser, function () {
                browser.maximizeWindow()
                    .deleteCookies()
                    .url(URL);
                browser.timeoutsImplicitWait(30000);
                LoginPage.waitForElementVisible('@PensionUsername', data.longWait);
            });
        }
    });

    this.When(/^User enter username and password and hit the login button$/, function () {
        var URL;
        browser = this;
        var execEnv = data["TestingEnvironmentPension"];
        if (execEnv.toUpperCase() === "QAI") {
            LoginPage.waitForElementVisible('@PensionPassword', data.longWait)
                .setValue('@PensionUsername', data.PensionUsername)
                .setValue('@PensionPassword', data.PensionPassword)
                .click('@ButtonLoginPension');
        }
    });

    this.Then(/^MBC Dashboard page should be displayed$/, function () {
        var URL;
        browser = this;
        var execEnv = data["TestingEnvironmentPension"];
        if (execEnv.toUpperCase() === "QAI") {
            DashboardPagePension.waitForElementVisible('@DashboardTile', data.longWait)
        }
    });

    this.Then(/User should logout of the application$/, function () {
        DashboardPagePension.click('@DBLogoutbutton');
        browser.end();
    });
    this.Given(/^User Opens the MBC Login screen$/, function () {
        var URL;
        browser = this;
        var execEnv = data["TestingEnvironmentPension"];
        console.log('Test Environment: ' + execEnv);
        if (execEnv.toUpperCase() === "QAI") {
            URL = data.PensionURL;
            initializePageObjects(browser, function () {
                browser.maximizeWindow()
                    .deleteCookies()
                    .url(URL);
                browser.timeoutsImplicitWait(30000);
                LoginPage.waitForElementVisible('@PensionUsername', data.longWait);
            });
        }});
        this.When(/^User enter Incorrect username and password and hit the login button$/, function () {
            var URL;
            browser = this;
            var execEnv = data["TestingEnvironmentPension"];
            if (execEnv.toUpperCase() === "QAI") {
                LoginPage.waitForElementVisible('@PensionPassword', data.longWait)
                    .setValue('@PensionUsername', data.IncorrectPensionUsername)
                    .setValue('@PensionPassword', data.IncorrectPensionPassword)
                    .click ('@ButtonLoginPension');
            }
        });
        this.Then(/^Error message should be displayed$/, function () {
            var URL;
            browser = this;
            var execEnv = data["TestingEnvironmentPension"];
            if (execEnv.toUpperCase() === "QAI") {
                LoginPage.waitForElementVisible('@LoginError', data.longWait)
                }
        });
    this.Given(/^User Opens the Login screen cookie$/, function () {
        var URL;
        browser = this;
        var execEnv = data["TestingEnvironmentPension"];
        console.log('Test Environment: ' + execEnv);
        if (execEnv.toUpperCase() === "QAI") {
            URL = data.PensionURL;
            initializePageObjects(browser, function () {
                browser.maximizeWindow()
                    .deleteCookies()
                    .url(URL);
                browser.timeoutsImplicitWait(30000);
                LoginPage.waitForElementVisible('@PensionUsername', data.longWait);
            });
        }
    });

    this.When(/^User reaches the MBC Dashboard screen.Logout from the application and click on browser back$/, function () {
        var URL;
        browser = this;
        var execEnv = data["TestingEnvironmentPension"];
        if (execEnv.toUpperCase() === "QAI") {
            LoginPage.waitForElementVisible('@PensionPassword', data.longWait)
                .setValue('@PensionUsername', data.PensionUsername)
                .setValue('@PensionPassword', data.PensionPassword)
                .click('@ButtonLoginPension');
            DashboardPagePension.waitForElementVisible('@DashboardTile', data.longWait)
                    }
    });

    this.Then(/^User should remain in the MBC Login screen$/, function () {
        var URL;
        browser = this;
        var execEnv = data["TestingEnvironmentPension"];
        if (execEnv.toUpperCase() === "QAI") {
            DashboardPagePension.waitForElementVisible('@DashboardTile', data.shortWait);
            DashboardPagePension.waitForElementVisible('@DBLogoutbutton', data.shortWait)
                .click('@DBLogoutbutton');
            browser.pause(data.LoginWait);
            LoginPage.waitForElementVisible('@PensionUsername', data.shortWait);
            this.back();
            browser.assert.elementNotPresent('@PensionUsername');
            }
    });
    this.Given(/^User Opens the Login screen timeout$/, function () {
        var URL;
        browser = this;
        var execEnv = data["TestingEnvironmentPension"];
        console.log('Test Environment: ' + execEnv);
        if (execEnv.toUpperCase() === "QAI") {
            URL = data.PensionURL;
            initializePageObjects(browser, function () {
                browser.maximizeWindow()
                    .deleteCookies()
                    .url(URL);
                browser.timeoutsImplicitWait(30000);
                LoginPage.waitForElementVisible('@PensionUsername', data.longWait);
            });
        }
    });

    this.When(/^User reaches the MBC Dashboard screen,copies the URL.Logout from the application and click on browser back$/, function () {
        var URL;
        browser = this;
        var execEnv = data["TestingEnvironmentPension"];
        if (execEnv.toUpperCase() === "QAI") {
            LoginPage.waitForElementVisible('@PensionPassword', data.longWait)
                .setValue('@PensionUsername', data.PensionUsername)
                .setValue('@PensionPassword', data.PensionPassword)
                .click('@ButtonLoginPension');
            DashboardPagePension.waitForElementVisible('@DashboardTile', data.longWait)
        }
    });
    this.When(/^Paste the Dashboard URL$/, function () {
        //urlcheck(browser)
        browser.url(function(result) {
            Ucopy=result.value;
            console.log(Ucopy);
        });
        DashboardPagePension.waitForElementVisible('@DBLogoutbutton', data.shortWait)
            .click('@DBLogoutbutton');
        browser.pause(data.shortWait);
        LoginPage.waitForElementVisible('@PensionUsername', data.shortWait);
        browser.pause(4000);
        browser.url(Ucopy);
    });
        this.Then(/^User should remain in the MBC Login timeout$/, function () {

        browser = this;
        browser.pause(10000);
            LoginPage.waitForElementVisible('@PensionUsername', data.shortWait);
        }
    );
    this.Given(/^User Opens the MBC Login screen Default credentials$/, function () {
        var URL;
        browser = this;
        var execEnv = data["TestingEnvironmentPension"];
        console.log('Test Environment: ' + execEnv);
        if (execEnv.toUpperCase() === "QAI") {
            URL = data.PensionURL;
            initializePageObjects(browser, function () {
                browser.maximizeWindow()
                    .deleteCookies()
                    .url(URL);
                browser.timeoutsImplicitWait(30000);
                LoginPage.waitForElementVisible('@PensionUsername', data.longWait);
            });
        }});
    this.When(/^User enter default username and password and hit the login button$/, function () {
        var URL;
        browser = this;
        var execEnv = data["TestingEnvironmentPension"];
        if (execEnv.toUpperCase() === "QAI") {
            LoginPage.waitForElementVisible('@PensionPassword', data.longWait)
                .setValue('@PensionUsername', data.DefaultPensionUsername)
                .setValue('@PensionPassword', data.DefaultPensionPassword)
                .click ('@ButtonLoginPension');
        }
    });
    this.Then(/^Error message should be displayed for Default credentials$/, function () {
        var URL;
        browser = this;
        var execEnv = data["TestingEnvironmentPension"];
        if (execEnv.toUpperCase() === "QAI") {
            //LoginPage.waitForElementVisible('@LoginError', data.longWait)
            browser.useXpath().getText('//span[@title=\'Warning\']/../../../../td[2]/div/table/tbody/tr/td[2]',function(result) {
                ErrorPrint=result.value;
                console.log(ErrorPrint);
            });
            }
    });
    this.Given(/^User Opens the MBC Login screen - lockout$/, function () {
        var URL;
        browser = this;
        var execEnv = data["TestingEnvironmentPension"];
        console.log('Test Environment: ' + execEnv);
        if (execEnv.toUpperCase() === "QAI") {
            URL = data.PensionURL;
            initializePageObjects(browser, function () {
                browser.maximizeWindow()
                    .deleteCookies()
                    .url(URL);
                browser.timeoutsImplicitWait(30000);
                //LoginPage.waitForElementVisible('@PensionUsername', data.longWait);
                //LoginPage.waitForElementVisible('@PensionUsername', data.longWait);
            });
        }});

    this.When(/^User enter invalid credential and the user is locked$/, function () {
        //LoginPage.waitForElementVisible('@PensionPassword', data.longWait);
                for(i=1;i<=5;i++)
                {
                    LoginPage.setValue('@PensionUsername', data.LockoutPensionUsername);
                    LoginPage.setValue('@PensionPassword', data.LockoutPensionPassword);
                    LoginPage.click ('@ButtonLoginPension');
                    if (i==3){
                        //ForgotpasswordPension.waitForElementVisible('@Title',data.shortWait);
                       ForgotpasswordPension.click('@Cancelbutton');
                       browser.pause(1000);
                       browser.acceptAlert();
                       LoginPage.waitForElementVisible('@PensionUsername', data.shortWait)
                    }
                }
                });
    this.Then(/^User enter the same username and password user should not be allowed to enter the application$/, function () {
            //LoginPage.waitForElementVisible('@LoginError', data.longWait)
            browser.useXpath().getText('//span[@title=\'Warning\']/../../../../td[2]/div/table/tbody/tr/td[2]',function(result) {
                ErrorPrint=result.value;
                console.log(ErrorPrint);
                browser.end();
            });

    });
    this.Given(/^User Opens the MBC Login screen - response$/, function () {
        var URL;
        browser = this;
                    initializePageObjects(browser, function () {
                browser.maximizeWindow()
                    .deleteCookies()
                    .url(URL);
                browser.timeoutsImplicitWait(30000);
                //LoginPage.waitForElementVisible('@PensionUsername', data.longWait);
                //LoginPage.waitForElementVisible('@PensionUsername', data.longWait);
            });
        });
    this.When(/^Get response header$/, function () {
        //LoginPage.waitForElementVisible('@PensionPassword', data.longWait);
        browser =this;
        var URL= data.PensionURL;
        var request = require('request');

        request(URL, function (error, response, body) {
            if (!error && response.statusCode == 200) {
                console.log(response.statusCode);
            }
            });
        });
    this.Then(/^verify the status code of the response header$/, function () {
        browser.end();
        });

    this.Given(/^User Opens the MBC Login screen - upload$/, function () {
        var URL;
        browser = this;
        var execEnv = data["TestingEnvironmentPension"];
        console.log('Test Environment: ' + execEnv);
        if (execEnv.toUpperCase() === "QAI") {
            URL = data.PensionURL;
            initializePageObjects(browser, function () {
                browser.maximizeWindow()
                    .deleteCookies()
                    .url(URL);
                browser.timeoutsImplicitWait(30000);
            });
        }});
    this.When(/^User upload the document in wrong format$/, function () {
        var URL;
        browser = this;
        var execEnv = data["TestingEnvironmentPension"];
        if (execEnv.toUpperCase() === "QAI") {
            LoginPage.waitForElementVisible('@PensionPassword', data.longWait)
                .setValue('@PensionUsername', data.uploadPensionUsername)
                .setValue('@PensionPassword', data.uploadPensionPassword)
                .click('@ButtonLoginPension');
            DashboardPagePension.waitForElementVisible('@DashboardTile', data.longWait);
            if(DashboardPagePension.waitForElementVisible('@KelloggHeritageTile',data.shortWait)){
                DashboardPagePension.click('@Resumebutton')
            }
            //if(ConfirmationpagePension.isEnabled(ConfirmationpagePension.Requireddocumentnav)){
                ConfirmationpagePension.click('@Requireddocumentnav');
                ConfirmationpagePension.waitForElementVisible('@Spousalconsentbox', data.shortWait);
                ConfirmationpagePension.click('@Spousalcheckbox');
                ConfirmationpagePension.waitForElementVisible('@Requireddocuploadtextbox', data.longWait);
                ConfirmationpagePension.click('@Requiredselectfilebutton');
                this.demoTest = function (browser) {
                    browser.moveToElement('@Requiredselectfilebutton', 10, 10);
                };
                browser.pause(5000);
                //ConfirmationpagePension.click('@Requiredselectfilebutton');
                browser.setValue('input[type=file]',require('path').resolve('C:\\Users\\Ruba-Krishnasamy\\Desktop\\Jagging.html'));
                browser.pause(5000);
                ConfirmationpagePension.click('@Requireduploadbutton');
                browser.pause(5000);

            //}
        }
    });
    this.Then(/^verify the error message displayed in the screen$/, function () {
        ConfirmationpagePension.waitForElementVisible('@Requirederrormessage', data.longWait);
        browser.useXpath().getText('//span[contains(text(),\'Upload unsuccessful. This file type cannot be processed. Allowed file types are PNG, PDF, GIF, TIF or TIFF, BMP, JPG, or JPEG\')]',function(result) {
            ErrorPrint=result.value;
            console.log(ErrorPrint);
            browser.end();
        });
    });

    this.Given(/^URL is identified for the test$/, function () {
        var URL;
        browser = this;
        URL = data.Jaggingfile;
        initializePageObjects(browser, function () {
            browser.maximizeWindow()
                .deleteCookies()
                .url(URL);
            browser.timeoutsImplicitWait(30000);
        });
    });

    this.When(/^save the URL in the file and click on the file saved in the desired location$/, function () {
        var URL;
        browser = this;
        URL = data.Jaggingfile;
        initializePageObjects(browser, function () {
            browser.maximizeWindow()
                .deleteCookies()
                .url(URL);
            browser.timeoutsImplicitWait(30000);
        });
    });

    this.Then(/^verify that the application is not opening in the frame$/, function () {
        browser.pause(15000);
        browser.assert.elementNotPresent('@PensionUsername')
        }
        );

    this.Given(/^User Opens the MBC Login screen for SQL Injection$/, function () {
        var URL;
        browser = this;
        URL = data.PensionURL;
        initializePageObjects(browser, function () {
            browser.maximizeWindow()
                .deleteCookies()
                .url(URL);
            browser.timeoutsImplicitWait(30000);
        });
    });

    this.When(/^User enter correct username and Injected password and hit the login button$/, function () {
        var URL;
        browser = this;
        var execEnv = data["TestingEnvironmentPension"];
        if (execEnv.toUpperCase() === "QAI") {
            LoginPage.waitForElementVisible('@PensionPassword', data.longWait)
                .setValue('@PensionUsername', data.LockoutPensionUsername)
                .setValue('@PensionPassword', data.SQLInjectionpassword)
                .click ('@ButtonLoginPension');
        }
    });

    this.Then(/^Error message should be displayed to show SQL injection is not allowed$/, function () {
        browser.useXpath().getText('//span[@title=\'Warning\']/../../../../td[2]/div/table/tbody/tr/td[2]',function(result) {
            ErrorPrint=result.value;
            console.log(ErrorPrint);
            browser.end();
        });
        }
    );
    this.Given(/^User login the MBC application for verifying the cross site$/, function () {
        var URL;
        browser = this;
        URL = data.PensionURL;
        initializePageObjects(browser, function () {
            browser.maximizeWindow()
                .deleteCookies()
                .url(URL);
            browser.timeoutsImplicitWait(30000);
        });
    });

    this.When(/^User navigates to profile page enter the XSS script$/, function () {
        var URL;
        browser = this;
        var execEnv = data["TestingEnvironmentPension"];
        if (execEnv.toUpperCase() === "QAI") {
            LoginPage.waitForElementVisible('@PensionPassword', data.longWait)
                .setValue('@PensionUsername', data.uploadPensionUsername)
                .setValue('@PensionPassword', data.uploadPensionPassword)
                .click ('@ButtonLoginPension');
            }
        ProfilepagePension.waitForElementVisible('@MenuButtonprofile', data.LoginWait);
        this.pause(5000);
        ProfilepagePension.click('@MenuButtonprofile');
        ProfilepagePension.waitForElementVisible('@DBProfilelink', 1000)
            .click('@DBProfilelink');
        ProfilepagePension.waitForElementVisible('@DBEditbuttoninProfilepage', data.shortWait)
            .click('@DBEditbuttoninProfilepage');
        this.pause(5000);
    });
        this.When(/^verify the error message$/, function () {

        ProfilepagePension.waitForElementVisible('@DBphone', data.shortWait)
            .clearValue('@DBphone')
            .setValue('@DBphone', data.DBPhonetext);
        ProfilepagePension.click('@DBemail');
        browser.useXpath().assert.containsText("/html/body/div[3]/div[3]/div/div/div[2]/div/div/div[2]/div/div/div/div[4]/div[2]/div[1]/div/div/div/div/div[2]", "You've exceeded the maximum characters allowed.");
        this.pause(5000);
    });

    this.Then(/User should logout of the applications$/, function () {
        DashboardPagePension.click('@DBLogoutbutton');
        browser.end();
    });
};



